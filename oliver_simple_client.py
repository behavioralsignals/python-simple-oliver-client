import argparse
import requests
import json
import logging
import jsonschema

logging.basicConfig(level=logging.INFO)


DEFAULT_CALL_DIRECTION = 1

JSON_SCHEMA_DEFAULT_PATH = './input_audio_diarization_schema.json'


def diarization_data_validator(data, schema):
    """
    data: diarization data to be validated
    schema: json schema definition
    """

    try:
        v = jsonschema.Draft4Validator(json.loads(json.dumps(schema)))
        v.check_schema(schema)

        for error in v.iter_errors(json.loads(json.dumps(data))):
            raise error
    except Exception as e:
        return e

    return None


def load_json_from_file(filepath):
    with open(filepath) as file:
        data = json.load(file)

    return data


def authorized(apiurl, apiid, apitoken):

    url = f"{apiurl}/auth"

    headers = {
        'X-Auth-Client': apiid,
        'X-Auth-Token': apitoken,
        'Accept': "application/json"
    }

    try:
        r = requests.get(url, headers=headers)
        r.raise_for_status()
    except Exception:
        return False

    return True


def create_tasks(diarization):

    tasks = {
        "tasks": [
            {
                "name": "diarization",
                "options": {
                    "activate": True,
                    "data": diarization
                }
            }
        ]
    }

    return json.dumps(tasks)


def submit_for_processing(apiurl, apiid, apitoken, files, channels, calldirection=DEFAULT_CALL_DIRECTION):

    url = f"{apiurl}/client/{apiid}/process/audio"

    params = {
        "calldirection": calldirection,
        "channels": channels,
    }

    headers = {
        "Accept": "application/json",
        "X-Auth-Token": apitoken
    }

    logging.info(f"Submitting audio for processing to Oliver API")

    r = requests.post(url, files=files, headers=headers, params=params)
    r.raise_for_status()

    logging.info(f"Sucessfully submitted audio to Oliver API, response: {r.json()}")


def parse_arguments():
    """!
    @brief Parse Arguments
    """

    parser = argparse.ArgumentParser(
        description="Simple CLI to submit audio with diarization data to Oliver API"
    )

    parser.add_argument(
        "-s",
        "--servername",
        type=str,
        required=True,
        help="Oliver API URL",
    )

    parser.add_argument(
        "-i",
        "--apiid",
        type=str,
        required=True,
        help="Client account ID"
    )

    parser.add_argument(
        "-t",
        "--apitoken",
        type=str,
        required=True,
        help="Client Token"
    )

    parser.add_argument(
        "-d",
        "--diarization",
        type=str,
        help="Path to the diarization data stored as a JSON file"
    )

    parser.add_argument(
        "-c",
        "--channels",
        type=int,
        required=True,
        help="Number of channels in audio"
    )

    parser.add_argument(
        "-a",
        "--audio",
        type=str,
        required=True,
        help="Path to the audio file to submit to Oliver API",
    )
    parser.add_argument(
        "--diar_schema",
        type=str,
        default=JSON_SCHEMA_DEFAULT_PATH,
        required=False,
        help="Path to the json schema definition of diarization data",
    )

    return parser


def main():
    parser = parse_arguments()

    args = parser.parse_args()

    diarization = None

    try:
        files = {
            'file': open(args.audio, 'rb')
        }
    except FileNotFoundError:
        logging.info(f"Not found file in {args.audio}")
        return
    except IOError:
        logging.info(f"Failed to read audio file {args.audio}")
        return
    except Exception as e:
        logging.info(f"Failed to access {args.audio}, error: {e}")
        return

    if args.diarization:
        diarization = load_json_from_file(args.diarization)

        try:
            diarization_schema = load_json_from_file(args.diar_schema)
        except Exception as e:
            logging.info(f"Not loaded schema {args.diar_schema} for diarization data,  error: {e}")
            return

        error = diarization_data_validator(diarization, diarization_schema)

        if error:
            logging.info(f"Provided diarization data failed to validate to schema, error: {error}")
            return

        files["data"] = create_tasks(diarization)

    if diarization and args.channels != 1:
        logging.info("External diarization provided for multichannel audio.")
        logging.info("For the time being Oliver API supports only single channel audio with external diarization.")
        return

    if not authorized(args.servername, args.apiid, args.apitoken):
        logging.info("Failed to authenticate request. No valid credentials provided!")
        return

    submit_for_processing(
        args.servername,
        args.apiid,
        args.apitoken,
        files,
        args.channels
    )


if __name__ == '__main__':
    main()
