### Oliver API Simple Submit Client

This is a simple client used to sumbit audio files for processing to Oliver API.

It can submit an audio file for processing to Oliver API with the option to include diarization data.

If provided the diarization data should be stored in a `.json` file following the schema defined in `input_audio_diarization_schema.json`


**Implemented in Python 3 version 3.6**

### How to install requirements
```bash
pip3 install -r requirements.txt
```

### How to submit audio without diarization data
```bash
python3 oliver_simple_client.py -s <oliver_api_url> -i <cid> -t <token> -a </path/to/audio/file> -c <channels>
```
Example <oliver_api_url>: https://api.behavioralsignals.com

### How to submit audio with diarization
```bash
python3 oliver_simple_client.py -s <oliver_api_url> -i <cid> -t <token> -d </path/to/audio/diarization.json> -a </path/to/audio/file> -c <channels>
```
Example <oliver_api_url>: https://api.behavioralsignals.com

### Get Help
```bash
python3 oliver_simple_client.py -h
```
